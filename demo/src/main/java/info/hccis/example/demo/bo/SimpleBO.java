package info.hccis.example.demo.bo;

/**
 * A simple business object to be the implementation of the model
 * @author bjmaclean
 * @since 20210909
 */
public class SimpleBO {
    
    private static int counter = 0;
    
    public static int processCounter(){
        ++counter;
        System.out.println("processing from the model");
        return counter;
    }
    
}
