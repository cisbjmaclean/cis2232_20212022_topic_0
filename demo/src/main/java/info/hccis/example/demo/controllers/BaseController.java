package info.hccis.example.demo.controllers;

import info.hccis.example.demo.bo.SimpleBO;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class BaseController {

    @RequestMapping("/")
    public String home() {

        int currentCounter = SimpleBO.processCounter();
        System.out.println("back in the controller.  counter=" + currentCounter);

        if (currentCounter > 3) {
            return "finished";
        } else {
            return "welcome";
        }
    }

}
